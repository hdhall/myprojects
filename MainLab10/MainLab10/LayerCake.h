#pragma once
#ifndef LAYERCAKE_H
#define LAYERCAKE_H
#include "Cake.h"

using namespace std;

class LayerCake : public Cake
{
public:
	LayerCake(string newCakeFlavor, string newFrostingFlavor, int numLayers, double newPrice = 9.00);
	string ToString();
	double GetPrice(int numItem);
protected:
	int numLayers;
};

#endif