#include "LayerCake.h"
#include <sstream>


LayerCake::LayerCake(string newCakeFlavor, string newFrostingFlavor, int newNumLayers, double newPrice)
{
	cakeFlavor = newCakeFlavor;
	frostingFlavor = newFrostingFlavor;
	numLayers = newNumLayers;
	price = newPrice;
	price += (numLayers - 1) * 3;
	if (frostingFlavor == "cream-cheese")
	{
		price += numLayers;
	}
}
string LayerCake::ToString()
{
	string outputString = "this works";
	ostringstream outputOSS;
	outputOSS << numLayers << "-layer " << cakeFlavor << " cake with " << frostingFlavor << " frosting ($" << BakedGood::ToString() << ")";
	outputString = outputOSS.str();
	return outputString;
}
double LayerCake::GetPrice(int numItem)
{
	double finalPrice = 0;
	double basePrice = BakedGood::GetPrice();
	if (numItem > 2)
	{
		basePrice -= 2;
	}
	finalPrice = basePrice * numItem;
	return finalPrice;
}