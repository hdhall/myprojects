#pragma once
#ifndef CUPCAKE_H
#define CUPCAK_H
#include <string>
#include "Cake.h"
#include "CupCake.h"

using namespace std;

class CupCake : public Cake
{
public:
	CupCake(string newCakeFlavor, string newFrostingFlavor, string newSprinkleFlavor, double newPrice = 1.95);
	string ToString();
	double GetPrice(int numItems);
protected:
	string sprinkleType;
};

#endif