#pragma once
#ifndef BREAD_H
#define BREAD_H
#include <string>
#include <iostream>
#include "BakedGood.h"

using namespace std;

class Bread : public BakedGood
{
public:
	Bread(string newType, double newPrice = 4.50);
	string ToString();
	double GetPrice(int numItems);
protected:
	string type;
};

#endif