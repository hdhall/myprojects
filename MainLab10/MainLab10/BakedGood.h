#pragma once
#ifndef BAKEDGOOD_H
#define BAKEDGOOD_H
#include <string>

using namespace std;

class BakedGood
{
public:
	BakedGood();
	void SetPrice(double newPrice);
	virtual string ToString();
	double GetPrice();
	virtual double GetPrice(int numItem);
protected:
	double price;
};

#endif