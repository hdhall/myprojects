#pragma once
#ifndef CAKE_H
#define CAKE_H
#include <string>
#include "BakedGood.h"

using namespace std;

class Cake : public BakedGood
{
public:
	Cake(string newFrostingFlavor = " ", string newCakeFlavor = " ");
protected:
	string frostingFlavor;
	string cakeFlavor;
};

#endif