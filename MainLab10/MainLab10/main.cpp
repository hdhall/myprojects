#include <iostream>
#include <string>
#include <iomanip>
#include <vector>

#include "BakedGood.h"
#include "Bread.h"
#include "Cake.h"
#include "CupCake.h"
#include "LayerCake.h"

using namespace std;

void MakeBread(vector<BakedGood*>& subOrderList);
void MakeCupCake(vector<BakedGood*>& subOrderList);
void MakeLayerCake(vector<BakedGood*>& subOrderList);
void PrintInvoice(vector<BakedGood*>& bakedGoodList);

int main()
{
	string userAction = "?";
	vector<BakedGood*> orderList;
	cout << "**Bread and Cakes Bakery**" << endl << endl;
	cout << "Enter sub-order (enter \"done\" to finish)" << endl;
	while (userAction != "done")
	{
		cout << "Sub-order: ";
		cin >> userAction;
		if (userAction == "Bread")
		{
			MakeBread(orderList);
		}
		if (userAction == "Layer-cake")
		{
			MakeLayerCake(orderList);
		}
		if (userAction == "Cupcake")
		{
			MakeCupCake(orderList);
		}
	}
	cout << "Order confirmations:" << endl << endl;
	for (unsigned int i = 0; i < orderList.size(); ++i)
	{
		cout << orderList.at(i)->ToString() << endl;
	}
	PrintInvoice(orderList);
	cout << "Good Bye";
	//system("pause");
	return 0;
}
void MakeBread(vector<BakedGood*>& subOrderList)
{
	string type;
	int numBread;
	Bread* newBread = nullptr;
	cin >> type;
	cin >> numBread;
	newBread = new Bread(type);
	for (int i = 0; i < numBread; ++i)
	{
		subOrderList.push_back(newBread);
	}
}
void MakeCupCake(vector<BakedGood*>& subOrderList)
{
	string cakeFlavor;
	string frostingFlavor;
	string sprinkleColor;
	int numCupCake;
	CupCake* newCupCake = nullptr;
	cin >> cakeFlavor;
	cin >> frostingFlavor;
	cin >> sprinkleColor;
	cin >> numCupCake;
	newCupCake = new CupCake(cakeFlavor, frostingFlavor, sprinkleColor);
	for (int i = 0; i < numCupCake; ++i)
	{
		subOrderList.push_back(newCupCake);
	}
}
void MakeLayerCake(vector<BakedGood*>& subOrderList)
{
	string cakeFlavor;
	string frostingFlavor;
	int numLayers;
	int numLayerCake;
	LayerCake* newLayerCake = nullptr;
	cin >> cakeFlavor;
	cin >> frostingFlavor;
	cin >> numLayers;
	cin >> numLayerCake;
	newLayerCake = new LayerCake(cakeFlavor, frostingFlavor, numLayers);
	for (int i = 0; i < numLayerCake; ++i)
	{
		subOrderList.push_back(newLayerCake);
	}
}
void PrintInvoice(vector<BakedGood*>& bakedGoodList)
{
	int itemCounter = 0;
	int itemTotal = 0;
	double priceTotal = 0;
	cout << "Invoice: " << endl;
	cout << left << setfill(' ') << setw(75) << "Baked Good" << setw(9) << "Quantity" << setw(9) <<  " Total" << endl;
	vector<BakedGood*> itemList;
	vector<int> numItemList;
	bool itemExists;
	//itemList.push_back(bakedGoodList.at(0));
	//numItemList.push_back(1);
	for (unsigned int i = 0; i < bakedGoodList.size(); ++i)
	{
		itemExists = false;
		for (unsigned int j = 0; j < itemList.size(); ++j)
		{
			if (bakedGoodList.at(i)->ToString() == itemList.at(j)->ToString())
			{
				itemExists = true;
			}
		}
		if (!itemExists)
		{
			itemList.push_back(bakedGoodList.at(i));
		}
	}
	for (unsigned int i = 0; i < itemList.size(); ++i)
	{
		itemCounter = 0;
		for (unsigned int j = 0; j < bakedGoodList.size(); ++j)
		{
			if (bakedGoodList.at(j)->ToString() == itemList.at(i)->ToString())
			{
				itemCounter++;
			}
		}
		numItemList.push_back(itemCounter);
		itemTotal += itemCounter;
	}
	for (unsigned int i = 0; i < itemList.size(); ++i)
	{
		priceTotal += itemList.at(i)->GetPrice(numItemList.at(i));
		cout << setw(75) << itemList.at(i)->ToString() << fixed << setprecision(2) << setw(9) << numItemList.at(i) << setw(9) << (itemList.at(i)->GetPrice(numItemList.at(i))) << endl;
	}
	cout << setw(75) << "Totals" << setw(9) << itemTotal << setw(9) << priceTotal << endl;
	//cout << endl << endl << itemTotal << "***" << priceTotal << endl;
}