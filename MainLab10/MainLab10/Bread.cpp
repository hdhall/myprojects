#include "Bread.h"
#include <string>
#include <sstream>

Bread::Bread(string newType, double newPrice)
{
	type = newType;
	price = newPrice;
}
string Bread::ToString()
{
	string outputString = "this works";
	ostringstream outputOSS;
	outputOSS << type << " bread ($" << BakedGood::ToString() << ")";
	outputString = outputOSS.str();
	return outputString;
}
double Bread::GetPrice(int numItem)
{
	const int DISCOUNT_NUM = 3;
	double basePrice = BakedGood::GetPrice();
	double finalPrice = 0;
	int discountCalc = numItem / DISCOUNT_NUM;
	if (numItem / DISCOUNT_NUM >= 1)
	{
		numItem -= discountCalc;
	}
	finalPrice = numItem * basePrice;
	return finalPrice;
}