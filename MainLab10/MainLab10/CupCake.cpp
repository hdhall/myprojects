#include "CupCake.h"
#include "Cake.h"
#include <sstream>

using namespace std;

CupCake::CupCake(string newCakeFlavor, string newFrostingFlavor, string newSprinkleFlavor, double newPrice)
{
	cakeFlavor = newCakeFlavor;
	frostingFlavor = newFrostingFlavor;
	price = newPrice;
	sprinkleType = newSprinkleFlavor;
	if (frostingFlavor == "cream-cheese")
	{
		price += .20;
	}
}
string CupCake::ToString()
{
	string outputString = "this works";
	ostringstream outputOSS;
	outputOSS << cakeFlavor << " cupcake with " << frostingFlavor << " frosting and " << sprinkleType << " sprinkles ($" << BakedGood::ToString() << ")";
	outputString = outputOSS.str();
	return outputString;
}
double CupCake::GetPrice(int numItem)
{
	double basePrice = BakedGood::GetPrice();
	double finalPrice = 0;
	if (numItem > 1)
	{
		basePrice -= .30;
	}
	if (numItem > 3)
	{
		basePrice -= .10;
	}
	finalPrice = numItem * basePrice;
	return finalPrice;
}