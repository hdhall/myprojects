#rock paper scissors game
import random as rand, sys
userWins = 0
userLosses = 0
userTies = 0
userInput = ''
helpCommand = 'h'
newGameCommand = '4'
exitCommand = 'e'
scoreCommand = 's'
rapidPlay = '9' #theoretically easy to create rapid play loop...but I don't know functions in python yet :/
helpMessage = 'Enter the following commands... \n 4: newgame \n e: exit \n s: score'
gameGuide = 'Enter one of the following commands... \n 1: ROCK \n 2: PAPER \n 3: SCISSORS'
newGameMessage = 'Alright make your move!'
compRock = ' vs. ROCK!'
compPaper = ' vs. PAPER!'
compSciss = ' vs. SCISSORS!'
compVic = 'You lose :/'
userVic = 'You win!'
scoreMessage = str('%s Wins, %s Losses, %s Ties' % (userWins, userLosses, userTies) )

print('Welcome to ROCK, PAPER, SCISSORS!')
print(helpMessage)
#print('Enter \'h\' at any time for help!')

while(userInput != exitCommand):
    userInput = input()
    if(userInput == helpCommand):
        print(helpMessage)
    elif userInput == scoreCommand:
        print(scoreMessage)
    elif(userInput == newGameCommand):
        compMove = rand.randint(1,3)
        userInput = 200
        #print(newGameMessage)
        while( int(userInput) > 3 or int(userInput) < 0):
            print(gameGuide)
            userInput = int(input())
            if(userInput == 1):
                print('ROCK ' , end = '')
            elif(userInput == 2):
                print('PAPER ', end = '')
            elif(userInput == 3):
                print('SCISSORS ', end = '')
            
            if(compMove == 1):
                print(compRock)
            elif(compMove == 2):
                print(compPaper)
            elif(compMove == 3):
                print(compSciss)
            #print('test...compmove: ' + str(compMove))
            
            if(userInput == compMove):
                print('Tie!')
                userTies += 1;
            elif(userInput == 1 and compMove == 2):
                print(compVic)
                userLosses += 1
            elif(userInput == 1 and compMove == 3):
                print(userVic)
                userWins += 1
            elif(userInput == 2 and compMove == 1):
                print(compVic)
                userLosses += 1
            elif(userInput == 2 and compMove == 3):
                print(userVic)
                userWins += 1
            elif(userInput == 3 and compMove == 1):
                print(compVic)
                userLosses += 1
            elif(userInput == 3 and compMove == 2):
                print(userVic)
                userWins += 1
            
            scoreMessage = str('%s Wins, %s Losses, %s Ties' % (userWins, userLosses, userTies) )
            print(helpMessage)
