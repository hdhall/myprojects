import time, random, copy

def printGrid(gridToPrint):
    print('\n*********************\n')
    for i in range(0,len(gridToPrint[0])):
        for j in range(0,len(gridToPrint)):
            print(gridToPrint[j][i], end = '')
        print()
    print('\n*********************\n')

def createNewGrid():
    userInput = ''
    newGrid = []
    while userInput != 'y':
        print('How big of a grid do you want to create?')
        print('Width: ', end = '')
        width = int(input())
        print('Height: ', end = '')
        height = int(input())
        print(str(width) + 'x' + str(height))
        newGrid.clear()
        for i in range(width):
            column = []
            for j in range(height):
                if random.randint(0,1) == 0:
                    column.append('#')
                else:
                    column.append(' ')
            newGrid.append(column)
        printGrid(newGrid)
        print('Enter \'y\' to accept this grid')
        userInput = input()
    return newGrid

def checkCell(gridToCheck, cellX, cellY):
    width = len(gridToCheck[0])
    height = len(gridToCheck)
    leftCoor = ( cellX - 1 ) % width
    rightCoor = ( cellX + 1 ) % width
    upCoor = ( cellY + 1 ) % height
    downCoor = ( cellY - 1 ) % height
    numLivingNeighbors = 0
    if gridToCheck[leftCoor][upCoor] == '#':
        numLivingNeighbors += 1
    if gridToCheck[cellX][upCoor] == '#':
        numLivingNeighbors += 1
    if gridToCheck[rightCoor][upCoor] == '#':
        numLivingNeighbors += 1
    if gridToCheck[leftCoor][cellY] == '#':
        numLivingNeighbors += 1
    if gridToCheck[rightCoor][cellY] == '#':
        numLivingNeighbors += 1
    if gridToCheck[leftCoor][downCoor] == '#':
        numLivingNeighbors += 1
    if gridToCheck[cellX][downCoor] == '#':
        numLivingNeighbors += 1
    if gridToCheck[rightCoor][downCoor] == '#':
        numLivingNeighbors += 1

    if gridToCheck[cellX][cellY] == '#' and (1 < numLivingNeighbors < 4 ):
        return True
    elif gridToCheck[cellX][cellY] == ' ' and ( numLivingNeighbors == 3 ):
        return True
    else:
        return False

def iterateGrid(gridToIterate):
    printOrNo = False
    print('Enter \'y\' to print the grid after each itteration')
    userInput = input()
    if userInput == 'y':
        printOrNo = True
    print('How many times would you like to iterate?')
    userInput = input()
    for i in range(int(userInput)):        
        newGrid = copy.deepcopy(gridToIterate)
        for i in range(0,len(gridToIterate[0])):
            for j in range(0,len(gridToIterate)):
                if checkCell(gridToIterate, j, i):
                    newGrid[j][i] = '#'
                else:
                    newGrid[j][i] = ' '
        gridToIterate = copy.deepcopy(newGrid)
        if printOrNo:
            printGrid(gridToIterate)
            time.sleep(1)
    if not printOrNo:
        printGrid(gridToIterate)
                
print('Welcome to Conway\'s Game of Life!')
while True:
    myGrid = []
    myGrid = createNewGrid()
    iterateGrid(myGrid)
    #myGrid = [ [' ',' ',' ',' '],[' ','#','#',' '],[' ','#','#',' '],[' ',' ',' ',' '] ]
