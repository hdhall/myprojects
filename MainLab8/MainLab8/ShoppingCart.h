#pragma once
#ifndef SHOPPINGCART_H
#define SHOPPINGCART_H

#include <string>
#include <vector>
#include "ItemToPurchase.h"

using namespace std;

class ShoppingCart
{
public:
	ShoppingCart();
	void SetCustomerName(string newName);
	string GetCustomerName();
	void SetCreationDate(string newDate);
	string GetCreationDate();
	void AddNewItem(ItemToPurchase itemToAdd);
	void RemoveItem(string itemToRemove);
	void UpdateItemQuantity();
	void GetItemQuantity();
	double GetTotalCost();
	int GetItemNum();
	void GetCartDescription();
	bool CheckForItem(string itemToCheck);
	void OutputCart();
private:
	vector<ItemToPurchase> CartItems;
	string customerName;
	string creationDate;
};

#endif