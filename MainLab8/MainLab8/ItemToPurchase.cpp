#include "ItemToPurchase.h"
#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

ItemToPurchase::ItemToPurchase()
{
	SetName("none");
	SetPrice(0.0);
	SetPrice(0);
	SetDescription("none");
}
void ItemToPurchase::SetName(string inputItemName)
{
	itemName = inputItemName;
}
string ItemToPurchase::GetName()
{
	return itemName;
}
void ItemToPurchase::SetPrice(double inputItemPrice = 0.0)
{
	itemPrice = inputItemPrice;
}
double ItemToPurchase::GetPrice()
{
	return itemPrice;
}
void ItemToPurchase::SetQuantity(int inputItemQuantity = 0)
{
	itemQuantity = inputItemQuantity;
}
int ItemToPurchase::GetQuantity()
{
	return itemQuantity;
}
void ItemToPurchase::SetDescription(string inputDescription = "none")
{
	itemDescription = inputDescription;
}
string ItemToPurchase::GetDescription()
{
	return itemDescription;
}
void ItemToPurchase::OutPutPrice()
{
	cout << itemName << " " << itemQuantity << " @ $ " << fixed << setprecision(2) << itemPrice << " = $" << itemPrice * itemQuantity << endl;
}
void ItemToPurchase::OutPutDescription()
{
	cout << itemName << ": " << itemDescription << endl;
}
