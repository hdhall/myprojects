#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

#include "ItemToPurchase.h"
#include "ShoppingCart.h"

//functions
ItemToPurchase CreateItem();
void OutputMenu();

int main()
{
	ShoppingCart userCart;
	string menuNav = "create";
	string customerName;
	string creationDate;

	cout << "Enter Customer's Name: ";
	getline(cin, customerName);
	userCart.SetCustomerName(customerName);
	cout << "Enter Today's Date: ";
	getline(cin, creationDate);
	userCart.SetCreationDate(creationDate);
	cout << "Enter option: ";
	cin >> menuNav;

	while (menuNav != "quit")
	{
		if (menuNav == "add")
		{
			ItemToPurchase newItem;
			newItem = CreateItem();
			userCart.AddNewItem(newItem);
		}
		else if (menuNav == "remove")
		{
			string itemToRemove;
			cin.ignore();
			cout << "Enter name of the item to remove: ";
			getline(cin, itemToRemove);
			userCart.RemoveItem(itemToRemove);
		}
		else if (menuNav == "change")
		{
			userCart.UpdateItemQuantity();
		}
		else if (menuNav == "descriptions")
		{
			userCart.GetCartDescription();
		}
		else if (menuNav == "cart")
		{
			userCart.OutputCart();
		}
		else
		{
			OutputMenu();
		}
		cout << endl << "Enter option: ";
		cin >> menuNav;
	}
	cout << "Goodbye.";
	return 0;
}

ItemToPurchase CreateItem()
{
	ItemToPurchase newItem;
	string itemDescription;
	double itemPrice;
	int itemQuantity;
	string itemName;
	cin.ignore();
	cout << "Enter the item name: ";
	getline(cin, itemName);
	cout << "Enter the item description: ";
	getline(cin, itemDescription);
	cout << "Enter the item price: ";
	cin >> itemPrice;
	cout << "Enter the item quantity: ";
	cin >> itemQuantity;
	newItem.SetName(itemName);
	newItem.SetDescription(itemDescription);
	newItem.SetPrice(itemPrice);
	newItem.SetQuantity(itemQuantity);
	return newItem;
}
void OutputMenu()
{
	cout << "MENU" << endl;
	cout << "add - Add item to cart" << endl;
	cout << "remove - Remove item from cart" << endl;
	cout << "change - Change item quantity" << endl;
	cout << "descriptions - Output items' descriptions" << endl;
	cout << "cart - Output shopping cart" << endl;
	cout << "options - Print the options menu" << endl;
	cout << "quit - Quit" << endl << endl;
}