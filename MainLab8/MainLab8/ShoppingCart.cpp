#include "ShoppingCart.h"
#include <iostream>
#include <string>
#include <iomanip>

using namespace std;


ShoppingCart::ShoppingCart()
{
	//cartItems = {};
	customerName = "none";
	creationDate = "January 1, 2016";
}
void ShoppingCart::SetCustomerName( string newName )
{
	customerName = newName;
}
string ShoppingCart::GetCustomerName()
{
	return customerName;
}
void ShoppingCart::SetCreationDate(string newDate)
{
	creationDate = newDate;
}
string ShoppingCart::GetCreationDate()
{
	return creationDate;
}
void ShoppingCart::AddNewItem(ItemToPurchase itemToAdd)
{
	if (!CheckForItem(itemToAdd.GetName()))
	{
		CartItems.push_back(itemToAdd);
	}
	else
	{
		cout << "Item is already in cart. Nothing added." << endl;
	}
}
void ShoppingCart::RemoveItem(string itemToRemove)
{
	if (!CheckForItem(itemToRemove))
	{
		cout << "Item not found in cart. Nothing removed." << endl;
	}
	ItemToPurchase checkerItem;
	for (unsigned int i = 0; i < CartItems.size(); ++i)
	{
		checkerItem = CartItems.at(i);
		if (checkerItem.GetName() == itemToRemove)
		{
			CartItems.erase(CartItems.begin() + i);
		}
	}
}
void ShoppingCart::UpdateItemQuantity()
{
	string itemToUpdate;
	int newQuantity;
	cin.ignore();
	cout << "Enter the item name: ";
	getline(cin, itemToUpdate);
	cout << "Enter the new quantity: ";
	cin >> newQuantity;
	if (CheckForItem(itemToUpdate))
	{
		ItemToPurchase checkerItem;
		for (unsigned int i = 0; i < CartItems.size(); ++i)
		{
			checkerItem = CartItems.at(i);
			if (checkerItem.GetName() == itemToUpdate)
			{
				checkerItem.SetQuantity(newQuantity);
				CartItems.at(i) = checkerItem;
			}
		}
	}
	else
	{
		cout << "Item not found in cart. Nothing modified." << endl;
	}
}
double ShoppingCart::GetTotalCost()
{
	double costCounter = 0;
	ItemToPurchase tempItem;
	for (unsigned int i = 0; i < CartItems.size(); ++i)
	{
		tempItem = CartItems.at(i);
		costCounter += tempItem.GetPrice()*tempItem.GetQuantity();
	}
	return costCounter;
}
int ShoppingCart::GetItemNum()
{
	int numItemCounter = 0;
	ItemToPurchase tempItem;
	for (unsigned int i = 0; i < CartItems.size(); ++i)
	{
		tempItem = CartItems.at(i);
		numItemCounter += tempItem.GetQuantity();
	}
	return numItemCounter;
}
void ShoppingCart::GetCartDescription()
{
	cout << customerName << "\'s Shopping Cart - " << creationDate << endl;
	if (CartItems.size() > 0)
	{
		cout << "Item descriptions" << endl;
		for (unsigned int i = 0; i < CartItems.size(); ++i)
		{
			ItemToPurchase tempItem = CartItems.at(i);
			tempItem.OutPutDescription();
		}
	}
	else
	{
		cout << "Shopping cart is empty." << endl;
	}
}
bool ShoppingCart::CheckForItem(string itemToCheck)
{
	bool itemExists = false;
	ItemToPurchase checkerItem;
	for (unsigned int i = 0; i < CartItems.size(); ++i)
	{
		checkerItem = CartItems.at(i);
		if (checkerItem.GetName() == itemToCheck)
		{
			itemExists = true;
		}
	}
	return itemExists;
}
void ShoppingCart::OutputCart()
{
	ItemToPurchase tempItem;
	cout << customerName << "\'s Shopping Cart - " << creationDate << endl;
	if (CartItems.size() > 0)
	{
		cout << "Number of items: " << GetItemNum() << endl << endl;
		for (unsigned int i = 0; i < CartItems.size(); ++i)
		{
			tempItem = CartItems.at(i);
			tempItem.OutPutPrice();
		}
		cout << endl;
		cout << "Total: $" << fixed << setprecision(2) << GetTotalCost() << endl;
	}
	else
	{
		cout << "Shopping cart is empty." << endl;
	}
}