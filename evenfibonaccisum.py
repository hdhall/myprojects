limit = 4000000
previousValue = 1
tempValue = 1
newValue = 2
total = 0

while newValue < limit:
    if newValue % 2 == 0:
        total += newValue
    tempValue = newValue
    newValue += previousValue
    previousValue = tempValue

print(total)
    
