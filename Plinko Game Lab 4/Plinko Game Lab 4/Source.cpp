#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>

using namespace std;

int main()
{
	//variables we will use
	int userInput = 3;
	int i = 0;
	int j = 0;
	int slotNumber;
	int randomValue;
	int finalSlot;
	int numberChips;
	const int LOOP_LENGTH = 12;
	double winningsAverage;
	double winningsTotal;
	double slotPath;
	double winningsTotalCounter = 0;

	srand(42);



	//prompt user
	cout << "Welcome to the Plinko simulator!  Enter 3 to see options." << endl << endl;
	cout << "Enter your selection now: " << endl;
	cin >> userInput;

	if (userInput == 4)
	{
		cout << "Goodbye!";
		return 0;
	}

	while (userInput != 4) {
		winningsTotalCounter = 0;
		//show menu when 3 is input by user
		if (userInput == 3)
		{
			cout << "Menu: Please select one of the following options: " << endl << endl;
			cout << "1 - Drop a single chip into one slot" << endl;
			cout << "2 - Drop multiple chips into one slot" << endl;
			cout << "3 - Show the options menu" << endl;
			cout << "4 - Quit the program" << endl;
			cin >> userInput;
		}
		else if (userInput == 4)
		{
			break;
		}
		else if (userInput == 1)
		{
			cout << "*** Drop a single chip ***" << endl << endl;
			cout << "Which slot do you want to drop the chip in (0-8)? " << endl;
			cin >> slotNumber;

			if (0 <= slotNumber && slotNumber <= 8)
			{
				cout << "*** Dropping chip into slot " << fixed << setprecision(0) << slotNumber << " ***" << endl;
				slotPath = slotNumber;

				cout << "Path: [ " << fixed << setprecision(1) << slotPath;
				for (i = 0; i < LOOP_LENGTH; ++i)
				{


					if (slotPath == 0)
					{
						slotPath += 0.5;
					}
					else if (slotPath == 8)
					{
						slotPath -= 0.5;
					}
					else
					{
						randomValue = rand() % 2;
						if (randomValue == 0)
						{
							slotPath -= 0.5;
						}
						else if (randomValue == 1)
						{
							slotPath += 0.5;
						}
					}
					cout << ", " << fixed << setprecision(1) << slotPath;
				}
				cout << " ]" << endl;

				finalSlot = slotPath;
				//cout << finalSlot << endl;
				switch (finalSlot)
				{
				case 0:
					winningsTotal = 100;
					break;
				case 1:
					winningsTotal = 500;
					break;
				case 2:
					winningsTotal = 1000;
					break;
				case 3:
					winningsTotal = 0;
					break;
				case 4:
					winningsTotal = 10000;
					break;
				case 5:
					winningsTotal = 0;
					break;
				case 6:
					winningsTotal = 1000;
					break;
				case 7:
					winningsTotal = 500;
					break;
				case 8:
					winningsTotal = 100;
					break;
				}
				cout << "Winnings: $" << fixed << setprecision(2) << winningsTotal << endl << endl;
			}
			else
			{
				cout << "Invalid slot." << endl << endl;
			}
		}
		//inform user of incorrect input
		else if (userInput == 2)
		{
			cout << " *** Drop multiple chips ***" << endl << endl;
			cout << "How many chips do you want to drop (>0)?" << endl << endl;
			cin >> numberChips;
			if (numberChips > 0)
			{
				cout << "Which slot do you want to drop the chip in (0-8)? " << endl;
				cin >> slotNumber;
				if (0 <= slotNumber && slotNumber <= 8)
				{
					for (i = 0; i < numberChips; ++i)
					{
						if (0 <= slotNumber && slotNumber <= 8)
						{
							slotPath = slotNumber;
							for (j = 0; j < LOOP_LENGTH; ++j)
							{
								if (slotPath == 0)
								{
									slotPath += 0.5;
								}
								else if (slotPath == 8)
								{
									slotPath -= 0.5;
								}
								else
								{
									randomValue = rand() % 2;
									if (randomValue == 0)
									{
										slotPath -= 0.5;
									}
									else if (randomValue == 1)
									{
										slotPath += 0.5;
									}
								}
							}
							finalSlot = slotPath;
							//cout << finalSlot << endl;
							switch (finalSlot)
							{
							case 0:
								winningsTotal = 100;
								break;
							case 1:
								winningsTotal = 500;
								break;
							case 2:
								winningsTotal = 1000;
								break;
							case 3:
								winningsTotal = 0;
								break;
							case 4:
								winningsTotal = 10000;
								break;
							case 5:
								winningsTotal = 0;
								break;
							case 6:
								winningsTotal = 1000;
								break;
							case 7:
								winningsTotal = 500;
								break;
							case 8:
								winningsTotal = 100;
								break;
							}
						}
						else
						{
							cout << "Invalid slot." << endl << endl;
							break;
						}
						winningsTotalCounter += winningsTotal;
					}

					winningsAverage = winningsTotalCounter / numberChips;
					cout << "Total winnings on " << numberChips << " chips: $" << fixed << setprecision(2) << winningsTotalCounter << endl;
					cout << "Average winnings per chip: $" << fixed << setprecision(2) << winningsAverage << endl;
					//winningsTotalCounter = 0;
				}
				else
				{
					cout << "Invalid slot." << endl;
				}
			}
			else
			{
				cout << "Invalid number of chips." << endl << endl;
			}
		}
		else
		{
			cout << "Invalid selection. Enter 3 to see options." << endl << endl;
		}
		cout << "Enter your selection now: " << endl << endl;

		cin >> userInput;
	}
	cout << "Goodbye!";

	return 0;
}