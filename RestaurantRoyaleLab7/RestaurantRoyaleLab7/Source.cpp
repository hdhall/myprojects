#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <limits>
#include <climits>

using namespace std;

void DisplayOptionsOption();
void DisplayRestaurantsOption(vector<string> restNames);
vector<string> AddOption(vector<string> restNames);
vector<string> RemoveOption(vector<string> restNames);
vector<string> CutOption(vector<string> restNames);
bool PowerOfTwo(int numberToCheck );
vector<string> ShuffleOption(vector<string> restNames);
vector<string> BattleOption(vector<string> restNames);
string getStringInput(string purpose);
int getIntInput(vector<string> restNames = { }, string purpose = "void");
bool CheckRestaurants(vector<string> restNames, string input, string purpose);

int main()
{
	vector<string>  restaurantNames;
	string menuInput = "options";

	cout << "Welcome to the restaurant battle! Enter \"options\" to see options." << endl << endl;

	while (menuInput != "quit")
	{
		menuInput = getStringInput("menuNav");
		if (menuInput == "quit")
		{
			break;
		}
		else if (menuInput == "options")
		{
			DisplayOptionsOption();
		}
		else if (menuInput == "add")
		{
			restaurantNames = AddOption(restaurantNames);
		}
		else if (menuInput == "remove")
		{
			restaurantNames = RemoveOption(restaurantNames);
		}
		else if (menuInput == "cut")
		{
			restaurantNames = CutOption(restaurantNames);
		}
		else if (menuInput == "shuffle")
		{
			restaurantNames = ShuffleOption(restaurantNames);
		}
		else if (menuInput == "battle")
		{
			restaurantNames = BattleOption(restaurantNames);
		}
		else if (menuInput == "display")
		{
			DisplayRestaurantsOption(restaurantNames);
		}
		else
		{
			cout << "Invalid selection" << endl;
			DisplayOptionsOption();
		}
	}

	cout << "Goodbye!";
	return 0;
}

void DisplayOptionsOption()
{
	cout << "Please select one of the following options: " << endl << endl;
	cout << "quit - Quit the program" << endl;
	cout << "display - Display all restaurants" << endl;
	cout << "add - Add a restaurant" << endl;
	cout << "remove - Remove a restaurant" << endl;
	cout << "cut - \"Cut\" the list of restaurants" << endl;
	cout << "shuffle - \"Shuffle\" the list of restaurants" << endl;
	cout << "battle - Begin the tournament" << endl;
	cout << "options - Print the options menu" << endl;
}
void DisplayRestaurantsOption(vector<string> restNames)
{
	cout << "Here are the current restaurants:" << endl << endl;
	for (unsigned i = 0; i < restNames.size(); ++i)
	{
		cout << "\t\"" << restNames.at(i) << "\"" << endl;

	}
	cout << endl;
}
vector<string> AddOption(vector<string> restNames)
{
	bool addOrNot = false;
	string userRestaurantAdd;
	cout << "What is the name of the restaurant you want to add?" << endl;
	userRestaurantAdd = getStringInput("addRest");
	addOrNot = CheckRestaurants(restNames, userRestaurantAdd, "addRest");
	if (addOrNot)
	{
		cout << userRestaurantAdd << " has been added." << endl;
		restNames.push_back(userRestaurantAdd);
	}
	return restNames;
}
vector<string> RemoveOption(vector<string> restNames)
{
	string input;
	bool remove = CheckRestaurants(restNames, input, "delRest");
	cout << "What is the name of the restaurant you want to remove?" << endl;
	input = getStringInput("delRest");
	remove = CheckRestaurants(restNames, input, "delRest");
	if (remove)
	{
		for (int i = 0; i < restNames.size(); ++i)
		{
			if (restNames.at(i) == input)
			{
				restNames.erase(restNames.begin() + i);
			}
		}
	}
	else
	{
		cout << "That restaurant is not on the list, you can not remove it." << endl;
	}
	return restNames;
}
vector<string> CutOption(vector<string> restNames)
{
	vector < string> cutVector = restNames;
	int cutIndex = 0;
	cout << "How many restaurants should be taken from the top and put on the bottom? " << endl << endl;
	cutIndex = getIntInput(restNames, "Cut");

	for (int i = 0; i < cutIndex; ++i) 
	{
		restNames.push_back(restNames.at(0));
		restNames.erase(restNames.begin()+0);
	}
	return restNames;
}
vector<string> ShuffleOption(vector<string> restNames)
{
	int counterOdds = 0;
	int counterEvens = 0;
	vector<string> shuffleVector = restNames;

	if (PowerOfTwo(restNames.size()))
	{
		for (int i = 0; i < restNames.size(); ++i)
		{
			if (i % 2 == 0)
			{
				shuffleVector.at(i) = restNames.at((restNames.size()/2) + counterEvens);
				counterEvens++;
			}
			else
			{
				shuffleVector.at(i) = restNames.at(counterOdds);
				counterOdds++;
			}
		}
	}
	else
	{
		cout << "The current tournament size (" << restNames.size() << ") is not a power of two (2, 4, 8...)." << endl;
		cout << "A shuffle is not possible. Please add or remove restaurants." << endl << endl;
	}
	return shuffleVector;
}
bool PowerOfTwo(int numberToCheck)
{
	bool powerOfTwo = false;
	for (int i = 0; i < numberToCheck; ++i)
	{
		int power = pow(2, i);
		if (power == numberToCheck)
		{
			powerOfTwo = true;
		}
	}
	return powerOfTwo;
}
bool CheckRestaurants(vector<string> restNames, string input, string purpose)
{
	bool addOrNot = true;
	for (int i = 0; i < restNames.size(); ++i)
	{
		if (purpose == "addRest")
		{
			if (restNames.at(i) == input)
			{
				cout << "That restaurant is already on the list, you can not add it again." << endl;
				addOrNot = false;
				break;
			}
		}
		if (purpose == "delRest")
		{
			addOrNot = false;
			if (restNames.at(i) == input)
			{
				addOrNot = true;
				cout << input << " has been removed." << endl;
				break;
			}
		}
	}
	return addOrNot;
}
string getStringInput(string purpose)
{
	string input;
	if (purpose == "menuNav")
	{
		cout << "Enter your selection: ";
		cin >> input;
		cout << endl;
	}
	if (purpose == "addRest" || purpose == "delRest")
	{
		cin.ignore();
		getline(cin, input);
	}
	return input;
}
int getIntInput(vector<string> restNames, string purpose)
{
	int input = 0;
	int vectorSize = restNames.size();
	cin >> input;
	if (purpose == "Cut")
	{
		while (cin.fail() || input < 0 || input > vectorSize)
		{
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			if (purpose == "Cut")
			{
				cout << "The cut number must be between 0 and " << vectorSize << endl;
				cout << "How many restaurants should be taken from the top and put on the bottom? " << endl << endl;
			}
			cin >> input;
			cout << endl;
		}
	}
	if (purpose == "Battle")
	{
		while (cin.fail() || input < 1 || input > 2)
		{
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			if (purpose == "Battle")
			{
				cout << "Invalid choice" << endl;
				return 0;
			}
			cin >> input;
			cout << endl;
		}
	}
	return input;
}
vector<string> BattleOption(vector<string> restNames)
{
	int i = 0;
	int choice = 0;
	int input = 0;
	int numRounds = 1;
	vector<string> battleVector;
	if (PowerOfTwo(restNames.size()))
	{
		while (restNames.size() > 1)
		{
			cout << "Round:" << numRounds << endl;
			while (i < restNames.size())
			{
				choice = 0;
				while (choice == 0)
				{
					cout << "Type \"1\" if you prefer " << restNames.at(i) << " or" << endl;
					cout << "Type \"2\" if you prefer " << restNames.at(i + 1) << endl;
					cout << "Choice: ";

					choice = getIntInput(restNames, "Battle");
				}
				if (choice == 1)
				{
					battleVector.push_back(restNames.at(i));
				}
				else
				{
					battleVector.push_back(restNames.at(i + 1));
				}
				i += 2;
			}
			numRounds++;
			restNames = battleVector;
			i = 0;
			battleVector.clear();
		}
		cout << "The winning restaurant is " << restNames.at(0) << "." << endl;
	}
	else
	{
		cout << "The current tournament size (" << restNames.size() << ") is not a power of two (2, 4, 8...)." << endl;
		cout << "A battle is not possible. Please add or remove restaurants." << endl;
	}
	return restNames;
}