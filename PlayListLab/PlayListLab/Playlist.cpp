#include "Playlist.h"
#include <iostream>
#include <ostream>
#include <string>
#include <iomanip>
using namespace std;

Playlist::Playlist(string startName)
{
	playlistName = startName;
}
void Playlist::AddSong(Song* songPtr)
{
	songPlaylist.push_back(songPtr);
}
string Playlist::GetName()
{
	return playlistName;
}
void Playlist::SetName(string newName)
{
	playlistName = newName;
}
void Playlist::PlaySongs()
{
	for (unsigned int i = 0; i < songPlaylist.size(); ++i)
	{
		songPlaylist.at(i)->PlaySong();
	}
}
vector< Song* > Playlist::GetSongList()
{
	return songPlaylist;
}
void Playlist::DeleteSong(int deleteIndex)
{
	songPlaylist.erase(songPlaylist.begin() + deleteIndex);
}
void Playlist::DeleteSong(Song* deleteSong)
{
	for (unsigned int i = 0; i < songPlaylist.size(); ++i)
	{
		if (songPlaylist.at(i) == deleteSong)
		{
			songPlaylist.erase(songPlaylist.begin() + i);
			i--;
		}
	}
}