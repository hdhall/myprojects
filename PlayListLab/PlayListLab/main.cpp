#include <iostream>
#include <string>
#include <iomanip>
#include <vector>
#include "Song.h"
#include  "Playlist.h"

using namespace std;

void OutputOptions();
void AddSong(vector<Song*>& songList);
void ListSongs(vector<Song*> songList);
void AddPlaylist(vector<Playlist>& playlistList);
void ListPlaylists(vector<Playlist> playlistList);
void ListSongTitles(vector<Song*> songList);
void AddSongToPlaylist(vector<Song*> songList, vector<Playlist>& playlistList);
void PlayPlaylist(vector<Playlist>& playlistList);
void DeletePlaylist(vector<Playlist>& playlistList);
void DeleteSongFromPlaylist(vector<Playlist>& playlistList);
void DeleteSongFromLibrary(vector<Playlist>& playlistList, vector<Song*>& songList);

int main()
{
	string userAction = "??";
	vector<Song*> songLibrary;
	vector<Playlist> playlistLibrary;
	Song* tmp = nullptr;

	cout << "Welcome to the Firstline Player! Enter options to see menu options." << endl << endl;
	cout << "Enter your selection now: ";
	cin >> userAction;
	cin.ignore();
	while (userAction != "quit")
	{
		if (userAction == "add")
		{
			AddSong(songLibrary);
		}
		else if (userAction == "list")
		{
			ListSongs(songLibrary);
		}
		else if (userAction == "addp")
		{
			AddPlaylist(playlistLibrary);
		}
		else if (userAction == "addsp")
		{
			AddSongToPlaylist(songLibrary, playlistLibrary);
		}
		else if (userAction == "listp")

		{
			ListPlaylists(playlistLibrary);
		}
		else if (userAction == "play")
		{
			PlayPlaylist( playlistLibrary );
		}
		else if (userAction == "delp")
		{
			DeletePlaylist(playlistLibrary);
		}
		else if (userAction == "delsp")
		{
			DeleteSongFromPlaylist(playlistLibrary);
		}
		else if (userAction == "delsl")
		{
			DeleteSongFromLibrary(playlistLibrary, songLibrary);
		}
		else
		{
			OutputOptions();
		}
		cout << "Enter your selection now: ";
		cin >> userAction;
		cin.ignore();
	}
	//we go through and delete all of the pointers in the songLibrary vector
	for (unsigned int i = 0; i < songLibrary.size(); ++i)
	{
		tmp = songLibrary.at(0);
		delete tmp;
		songLibrary.erase(songLibrary.begin() + 0);
	}
	cout << "Goodbye!";
	return 0;
}

void OutputOptions()
{
	cout << endl;
	cout << "add      Adds a list of songs to the library" << endl;
	cout << "list     Lists all the songs in the library" << endl;
	cout << "addp     Adds a new playlist" << endl;
	cout << "addsp    Adds a song to a playlist" << endl;
	cout << "listp    Lists all the playlists" << endl;
	cout << "play     Plays a playlist" << endl;
	cout << "delp     Deletes a playlist" << endl;
	cout << "delsp    Deletes a song from a playlist" << endl;
	cout << "delsl    Deletes a song from the library(and all playlists)" << endl;
	cout << "options  Prints this options menu" << endl;
	cout << "quit     Quits the program" << endl << endl;
}

void AddSong(vector<Song*>& songList)
{
	Song* newSong = nullptr;
	string songName = " ";
	string songFirstline;
	cout << "Read in Song names and first lines (type \"STOP\" when done):" << endl;
	while (songName != "STOP")
	{
		cout << "Song Name: ";
		getline(cin, songName);
		if (songName == "STOP")
		{
			break;
		}
		cout << "Song's first line: ";
		getline(cin, songFirstline);
		newSong = new Song(songName, songFirstline);
		songList.push_back(newSong);
	}
}
void ListSongs(vector<Song*> songList)
{
	cout << endl;
	for (unsigned int i = 0; i < songList.size(); ++i)
	{
		Song* temp;
		temp = songList.at(i);
		cout << temp->ToString();
	}
}
void AddPlaylist(vector<Playlist>& playlistList)
{
	string playlistName;
	cout << "Playlist name: ";
	getline(cin, playlistName);
	Playlist newPlaylist;
	newPlaylist.SetName(playlistName);
	playlistList.push_back(newPlaylist);
}
void ListPlaylists(vector<Playlist> playlistList)
{
	cout << endl;
	for (unsigned int i = 0; i < playlistList.size(); ++i)
	{
		cout << "  " << i << ": " << (playlistList.at(i)).GetName() << endl;
	}
}
void ListSongTitles(vector<Song*> songList)
{
	cout << endl;
	for (unsigned int i = 0; i < songList.size(); ++i)
	{
		Song temp;
		temp = *songList.at(i);
		cout << "  " << i << ": " << (temp.GetName()) << endl;
	}
}
void AddSongToPlaylist(vector<Song*> songList, vector<Playlist>& playlistList)
{
	int playlistIndex;
	int songIndex;
	ListPlaylists(playlistList);
	cout << "Pick a playlist index number: ";
	cin >> playlistIndex;
	ListSongTitles(songList);
	cout << "Pick a song index number: ";
	cin >> songIndex;
	Song* mySong = nullptr;
	mySong = (songList.at(songIndex));
	(playlistList.at(playlistIndex)).AddSong(mySong);
}
void PlayPlaylist(vector<Playlist>& playlistList)
{
	int playlistIndex;
	ListPlaylists(playlistList);
	cout << "Pick a playlist index number: ";
	cin >> playlistIndex;
	cout << endl;
	cout << "Playing first lines of playlist: " << playlistList.at(playlistIndex).GetName() << endl;
	playlistList.at(playlistIndex).PlaySongs();
}
void DeletePlaylist(vector<Playlist>& playlistList)
{
	int index;
	ListPlaylists(playlistList);
	cout << "Pick a playlist index number to delete: ";
	cin >> index;
	playlistList.erase(playlistList.begin() + index);
}
void DeleteSongFromPlaylist(vector<Playlist>& playlistList)
{
	int playlistIndex;
	int songIndex;
	ListPlaylists(playlistList);
	cout << "Pick a playlist index number: ";
	cin >> playlistIndex;
	ListSongTitles(playlistList.at(playlistIndex).GetSongList());
	cout << "Pick a song index number to delete: ";
	cin >> songIndex;
	cout << endl;
	playlistList.at(playlistIndex).DeleteSong(songIndex);
}
void DeleteSongFromLibrary(vector<Playlist>& playlistList, vector<Song*>& songList)
{
	int deleteIndex;
	ListSongTitles(songList);
	cout << "Pick a song index number: ";
	cin >> deleteIndex;
	Song* tempSong = songList.at(deleteIndex);
	for (unsigned int i = 0; i < playlistList.size(); ++i)
	{
		playlistList.at(i).DeleteSong(tempSong);
	}
	//clear memory for pointer in songLibrary here when a song is deleted from the library
	delete tempSong;
	songList.erase(songList.begin() + deleteIndex);
}