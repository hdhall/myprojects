#pragma once
#ifndef SONG_H
#define SONG_H
#include <string>
using namespace std;

class Song
{
public:
	Song(string firstName = "none", string firstFirstline = "none", int numplays = 0);
	void SetName(string newName);
	string GetName();
	void SetFirstline(string newFirstline);
	string GetFirstline();
	void PlaySong();
	int GetPlays();
	string ToString() const;
private:
	string songName;
	string firstline;
	int plays;
};

#endif