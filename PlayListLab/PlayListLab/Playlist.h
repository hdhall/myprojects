#pragma once
#ifndef PLAYLIST_H
#define PLAYLIST_H
#include <string>
#include <vector>
#include "Song.h"
using namespace std;

class Playlist
{
public:
	Playlist(string startName = "none");
	void AddSong(Song* songPtr);
	string GetName();
	void SetName(string newName);
	void PlaySongs();
	vector< Song* > GetSongList();
	void DeleteSong(int deleteIndex);
	void DeleteSong(Song* deleteSong);
private:
	vector< Song* > songPlaylist;
	string playlistName;
};

#endif