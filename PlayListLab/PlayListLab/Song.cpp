#include "Song.h"
#include <iostream>
#include <ostream>
#include <string>
#include <iomanip>
#include <sstream>
using namespace std;

Song::Song(string firstName, string firstFirstline, int numPlays )
{
	songName = firstName;
	firstline = firstFirstline;
	plays = numPlays;
}
void Song::SetName(string newName)
{
	songName = newName;
}
string Song::GetName()
{
	return songName;
}
void Song::SetFirstline(string newFirstline)
{
	firstline = newFirstline;
}
string Song::GetFirstline()
{
	return firstline;
}
void Song::PlaySong()
{
	plays++;
	cout << firstline << endl;
}
int Song::GetPlays()
{
	return plays;
}
string Song::ToString() const
{
	string returnString;
	ostringstream outputOSS;
	outputOSS << songName << ": \"" << firstline << "\", "<< plays << " play(s).\n";
	returnString = outputOSS.str();
	return returnString;
}