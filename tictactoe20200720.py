import random, copy
middleRows = '-+-+-'
userInput = ''
newGrid = { 1 : '1' , 2 : '2' , 3 : '3',
            4 : '4' , 5 : '5' , 6 : '6',
            7 : '7' , 8 : '8' , 9 : '9', }
def printGrid(gridToPrint):
    print(gridToPrint[1] + '|' + gridToPrint[2] + '|' + gridToPrint[3])
    print(middleRows)
    print(gridToPrint[4] + '|' + gridToPrint[5] + '|' + gridToPrint[6])
    print(middleRows)
    print(gridToPrint[7] + '|' + gridToPrint[8] + '|' + gridToPrint[9])
def setUpGrid(gridToSetup):
    playGrid = copy.copy(gridToSetup)
    for i in range(1,10):
        playGrid[i] = ' '
    return playGrid
def makeMove(turn, grid):
    placeToMove = 0
    print('Enter the value of the cell you want to make a move in: ', end = '')
    while placeToMove < 1 or placeToMove > 9:
        placeToMove = int(input())
        if placeToMove > 9 or placeToMove < 1:
            print('Enter a valid location: ', end = '')
        elif grid[placeToMove] == 'x' or grid[placeToMove] == 'o':
            print('Enter a valid location: ', end = '')
            placeToMove = 0
        else:
            grid[placeToMove] = turn
            break
    printGrid(grid)
def checkGrid(gridToCheck):
    if gridToCheck[1] == gridToCheck[2] == gridToCheck[3] != ' ':
        return True
    elif gridToCheck[1] == gridToCheck[5] == gridToCheck[9] != ' ':
        return True
    elif gridToCheck[1] == gridToCheck[4] == gridToCheck[7] != ' ':
        return True
    elif gridToCheck[4] == gridToCheck[5] == gridToCheck[6] != ' ':
        return True
    elif gridToCheck[7] == gridToCheck[8] == gridToCheck[9] != ' ':
        return True
    elif gridToCheck[2] == gridToCheck[5] == gridToCheck[8] != ' ':
        return True
    elif gridToCheck[3] == gridToCheck[6] == gridToCheck[9] != ' ':
        return True
    elif gridToCheck[3] == gridToCheck[5] == gridToCheck[7] != ' ':
        return True
def playGame(player):
    turn = 'x'
    placeToMove = 0
    printGrid(newGrid)
    gridToPlay = setUpGrid(newGrid)
    #print('Enter \'end\' to end the game.')
    print('Player 1 will be x\'s')
    for i in range(0,9):
        makeMove(turn, gridToPlay)
        if checkGrid(gridToPlay):
            print(turn + '\'s won!')
            break
        if turn == 'x':
            turn = 'o'
        else:
            turn = 'x'

print('***Welcome to Tic-Tac-Toe!***')
while(userInput != 'exit'):    
    print('*****************************')
    print('Enter \'c\' to play with a computer')
    print('Enter anything else to play with another person')
    print('*****************************')
    print('Enter \'exit\' to exit')
    print('*****************************')
    print()
    userInput = input()
    if(userInput == 'exit'):
        break
    if(userInput == 'c'):
        playGame('c')
    else:
        playGame('p')
print('Goodbye!')
