def collatzsequence(number):
    iterations = 0
    while number != 1:
        if number % 2 == 0:
            number = number // 2
        else:
            number = (number*3)+1
        print(number)
        iterations += 1
    return iterations 

userInput = 0
while(userInput != 'e'):
    userInput = input()
    try:
        print(collatzsequence(int(userInput)))
    except:
        print('Enter an integer')
