#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int main()
{
	const int NUM_ROWS = 10;
	const int NUM_COLS = 10;
	const double = 100.000;
	int hotPlate[NUM_ROWS][NUM_COLS];

	for (int i = 0; i < NUM_ROWS; ++i)
	{
		for (int j = 0; j < NUM_COLS; ++j)
		{
			if (i > 0 && (j == 1 || j == 9)
			{
				hotPlate[i][j] = 100.000;
			}
		}
	}
	for (int i = 0; i < NUM_ROWS; ++i)
	{
		for (int j = 0; j < NUM_COLS; ++j)
		{
			cout << hotPlate[i][j] << " ";
		}
		cout << endl;
	}
	return 0;
}