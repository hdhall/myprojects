#include "ItemToPurchase.h"
#include <iostream>
#include <string>

using namespace std;

ItemToPurchase::ItemToPurchase()
{
	SetName( "none");
	SetPrice( 0.0 );
	SetPrice( 0 );
}
void ItemToPurchase::SetName(string inputItemName = "none")
{
	itemName = inputItemName;
}
string ItemToPurchase::GetName()
{
	return itemName;
}
void ItemToPurchase::SetPrice(double inputItemPrice = 0.0 )
{
	itemPrice = inputItemPrice;
}
double ItemToPurchase::GetPrice()
{
	return itemPrice;
}
void ItemToPurchase::SetQuantity( int inputItemQuantity = 0)
{
	itemQuantity = inputItemQuantity;
}
int ItemToPurchase::GetQuantity()
{
	return itemQuantity;
}
