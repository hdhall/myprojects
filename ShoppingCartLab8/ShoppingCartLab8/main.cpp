#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

#include "ItemToPurchase.h"

int main()
{    
	ItemToPurchase item1;
	ItemToPurchase item2;
	string itemName;
	string itemName2;
	double itemPrice;
	int itemQuantity;
	double totalPrice;

	//item 1
	cout << "Item 1" << endl;
	cout << "Enter the item name: ";
	getline(cin, itemName);
	item1.SetName(itemName);

	cout << "Enter the item price: ";
	cin >> itemPrice;
	item1.SetPrice(itemPrice);
	
	cout << "Enter the item quantity: ";
	cin >> itemQuantity;
	cout << endl << endl;
	item1.SetQuantity(itemQuantity);

	//item 2
	cout << "Item 2" << endl;
	cout << "Enter the item name: ";
	cin.ignore();
	getline(cin, itemName2);
	item2.SetName(itemName2);

	cout << "Enter the item price: ";
	cin >> itemPrice;
	item2.SetPrice(itemPrice);

	cout << "Enter the item quantity: ";
	cin >> itemQuantity;
	cout << endl;
	item2.SetQuantity(itemQuantity);

	//total cost
	totalPrice = item1.GetPrice()*item1.GetQuantity() + item2.GetPrice()*item2.GetQuantity();
	cout << "TOTAL COST" << endl;
	cout << fixed << setprecision(2) << item1.GetName() << " " << item1.GetQuantity() << " @ $" << item1.GetPrice() << " = $" << item1.GetQuantity()*item1.GetPrice() << endl;
	cout << item2.GetName() << " " << item2.GetQuantity() << " @ $" << item2.GetPrice() << " = $" << item2.GetQuantity()*item2.GetPrice() << endl << endl;
	cout << "Total: $" << totalPrice;

	cin >> itemQuantity;

	return 0;
}