#pragma once
#ifndef ITEMTOPURCHASE_H
#define ITEMTOPURCHASE_H

#include <string>
using namespace std;

class ItemToPurchase
{
public:
	ItemToPurchase();
	void SetName( string inputItemName);
	string GetName();
	void SetPrice(double inputItemPrice);
	double GetPrice();
	void SetQuantity(int inputItemQuantity);
	int GetQuantity();
private:
	string itemName;
	double itemPrice;
	int itemQuantity;
};

#endif