def smallest_element(listtocheck):
    #finds the smallest element of a list as outlined in problem 1.13(i)
    smallestnum = listtocheck[0]
    for i in listtocheck:
        if smallestnum > i:
            smallestnum = i
    return smallestnum

#this was made to test my algorithm feel free to remove it. I don't care
'''
mylist = [1,2,3,4,5,0]
print(smallest_element(mylist))
'''
def selection_sort_slow_version(listtosort):
    #sorts a list as outlined in problem 1.14(ii)
    listtoreturn = []
    for i in range(0,len(listtosort)):
        listtoreturn.append( smallest_element(listtosort) )
        for j in range(0,len(listtosort)):
            if listtosort[j] == listtoreturn[i]:
                listtosort[j] = 1000000000
                break
    return listtoreturn

def selection_sort(listtosort):
    for i in range(listtosort):
        tempint = listtosort[i]
        listtosort[i] = smallest_element(listtosort[i:])

#test for selection_sort
mylist2 = [float(n) for n in range(10,10000)]
print(selection_sort(mylist2))
print("done")

