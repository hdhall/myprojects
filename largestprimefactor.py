numberToFactor = 20000
largestPrimeFactor = 0
factor = 1471
largestPrimeFactor = 2
primesArray = []
prime = 1

while factor < (numberToFactor - 1) / 2:
    prime = 1
    for i in range(2, factor ):
        if factor % i == 0:
            prime = 0
            break
        if i > factor / 2:
            break
    if prime == 1:
        primesArray.append(factor)
    factor += 2
#print("done")
for i in primesArray:
    if 600851475143 % i == 0:
        largestPrimeFactor = i
    
print(largestPrimeFactor)

"""
Henry Hall 4/28/2019
The program generates the output 6857 - the greatest prime factor of 600851475143
The program does not find all prime numbers up to 60085147514 but only from
    1471 up to 20000 and the greatest prime factor of just happens to fall in
    that range
"""
