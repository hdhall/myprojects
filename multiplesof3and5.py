multiple1 = 3
multiple2 = 5
counter = 1
limit = 1000
sum = 0

while counter < limit:
    if counter % multiple1 == 0 or counter % multiple2 == 0:
        sum += counter
    counter += 1

print(sum)
