#practicing flow controls
userInPut = ''
standardOutPut = 'Enter "help" for more options'
helpOutPut = 'Enter "end" to exit the program'
while userInPut != 'end':
    userInPut = input()
    if(userInPut == 'help'):
        print(helpOutPut)
    elif(userInPut == 'overide'):
        break
    else:
        print(standardOutPut)
print('Okay, closing this loop')
userInput = ''

while userInPut:
    print('what is your name')
    userInPut = input()
    if userInPut == 'Joe':
        continue
print('closing program')
